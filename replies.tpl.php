<?php if ($access <> REPLY_ACCESS_NONE): ?>
<div class="replies-wrapper">
  <div class="replies-header"><h3><?php print $header ?></h3></div>
  <div class="replies"><?php print render($replies) ?></div>
  <div class="replies-links"><?php print render($links) ?></div>
  <div class="replies-form"><?php print render($reply_form) ?></div>
</div>
<?php endif; ?>