<div id="reply-<?php print $reply->id ?>" class="<?php print $classes ?>">
  <div class="reply-subject"><?php print $subject ?></div>
  <div class="reply-name"><?php print $name ?></div>
  <div class="reply-date"><?php print $created ?></div>
  <div class="reply-body"><?php print $body; print render($content) ?></div>
  <div class="reply-links"><?php print render($links) ?></div>
</div>