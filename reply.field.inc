<?php


/**
 * _field_info
 */
function reply_field_info() {
  return array(
    'reply' => array(
      'label' => t('Reply'),
      'description' => t('Enables users to comment on this entity by reply module.'),
      'default_widget' => 'reply',
      'default_formatter' => 'reply_default',
      'entity_types' => array(),
      'no_ui' => FALSE,
    )
  );
}


/**
 *  _field_settings_form
 */
function reply_field_settings_form($field, $instance, $has_data) {
  $bundles = reply_load_bundles();
  $options = array();
  foreach ($bundles AS $bundle) {
    $options[$bundle->bundle] = $bundle->name;
  }

  $form['bundle'] = array(
    '#type' => 'select',
    '#title' => t('Reply bundle'),
    '#default_value' => isset($field['settings']['bundle']) ? $field['settings']['bundle'] : NULL,
    '#options' => $options,
    '#required' => TRUE,
    '#multiple' => FALSE,
  );
  $form['access'] = array(
    '#type' => 'radios',
    '#title' => t('Access'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_ACCESS_NONE => t('Disabled'),
      REPLY_ACCESS_READ => t('Read only'),
      REPLY_ACCESS_FULL => t('Read and write')
    ),
    '#default_value' => isset($field['settings']['access']) ? $field['settings']['access'] : REPLY_INHERIT
  );
  $form['display'] = array(
    '#type' => 'radios',
    '#title' => t('Display'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_LIST_FLAT => t('Flat list'),
      REPLY_LIST_TREE => t('Threaded list')
    ),
    '#default_value' => isset($field['settings']['display']) ? $field['settings']['display'] : REPLY_INHERIT
  );
  $form['form'] = array(
    '#type' => 'radios',
    '#title' => t('Form position'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_FORM_PAGE_SAME => t('On the same page'),
      REPLY_FORM_PAGE_CUSTOM => t('On custom page')
    ),
    '#default_value' => isset($field['settings']['form']) ? $field['settings']['form'] : REPLY_INHERIT
  );
  $form['subject'] = array(
    '#type' => 'radios',
    '#title' => t('Subject'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_SUBJECT_DISABLED => t('Hidden'),
      REPLY_SUBJECT_ALLOWED => t('Allowed'),
      REPLY_SUBJECT_REQUIRED => t('Required')
    ),
    '#default_value' => isset($field['settings']['subject']) ? $field['settings']['subject'] : REPLY_INHERIT
  );

  return $form;
}


/**
 *  _field_instance_settings_form
 */
function reply_field_instance_settings_form($field, $instance) {
  $form['access'] = array(
    '#type' => 'radios',
    '#title' => t('Access'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_ACCESS_NONE => t('Disabled'),
      REPLY_ACCESS_READ => t('Read only'),
      REPLY_ACCESS_FULL => t('Read and write')
    ),
    '#default_value' => isset($instance['settings']['access']) ? $instance['settings']['access'] : REPLY_INHERIT
  );
  $form['display'] = array(
    '#type' => 'radios',
    '#title' => t('Display'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_LIST_FLAT => t('Flat list'),
      REPLY_LIST_TREE => t('Threaded list')
    ),
    '#default_value' => isset($instance['settings']['display']) ? $instance['settings']['display'] : REPLY_INHERIT
  );
  $form['form'] = array(
    '#type' => 'radios',
    '#title' => t('Form position'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_FORM_PAGE_SAME => t('On the same page'),
      REPLY_FORM_PAGE_CUSTOM => t('On custom page')
    ),
    '#default_value' => isset($instance['settings']['form']) ? $instance['settings']['form'] : REPLY_INHERIT
  );
  $form['subject'] = array(
    '#type' => 'radios',
    '#title' => t('Subject'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_SUBJECT_DISABLED => t('Hidden'),
      REPLY_SUBJECT_ALLOWED => t('Allowed'),
      REPLY_SUBJECT_REQUIRED => t('Required')
    ),
    '#default_value' => isset($instance['settings']['subject']) ? $instance['settings']['subject'] : REPLY_INHERIT
  );

  return $form;
}


/**
 * _field_is_empty
 */
function reply_field_is_empty($item, $field) {
  return FALSE;
}

/**
 * _field_formatter_info
 */
function reply_field_formatter_info() {
  return array(
    'reply_default' => array(
      'label' => t('Default'),
      'field types' => array('reply'),
    )
  );
}


/**
 * _field_formatter_view
 */
function reply_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  $entity_info = entity_get_info($entity_type);
  $entity_id_key = $entity_info['entity keys']['id'];
  $entity_id = $entity->{$entity_id_key};

  foreach ($items as $delta => $item) {
    $settings = reply_settings($field['settings']['bundle'], $field['settings'], $instance['settings'], $item);
    $ids = reply_get_entity($entity_id, $entity_type, $instance['id']);
    reply_filter_disabled($ids);
    $settings['replies'] = reply_load_multiple($ids);
    $settings['instance_id'] = $instance['id'];
    $settings['entity_id'] = $entity_id;
    $settings['entity'] = $entity;

    $element[$delta] = array('#markup' => theme('replies', array('elements' => $settings)));
  }

  return $element;
}


/**
 * _field_widget_info
 */
function reply_field_widget_info() {
  return array(
    'reply' => array(
      'label' => t('Replies'),
      'field types' => array('reply'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    )
  );
}


/**
 * _field_widget_form()
 */
function reply_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element['#type'] = 'fieldset';
  $element['#title'] = t('Reply settings');

  $element['access'] = array(
    '#type' => 'radios',
    '#title' => t('Access'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_ACCESS_NONE => t('Disabled'),
      REPLY_ACCESS_READ => t('Read only'),
      REPLY_ACCESS_FULL => t('Read and write')
    ),
    '#default_value' => isset($items[$delta]['access']) ? $items[$delta]['access'] : REPLY_INHERIT
  );
  $element['display'] = array(
    '#type' => 'radios',
    '#title' => t('Display'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_LIST_FLAT => t('Flat list'),
      REPLY_LIST_TREE => t('Threaded list')
    ),
    '#default_value' => isset($items[$delta]['display']) ? $items[$delta]['display'] : REPLY_INHERIT
  );
  $element['form'] = array(
    '#type' => 'radios',
    '#title' => t('Form position'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_FORM_PAGE_SAME => t('On the same page'),
      REPLY_FORM_PAGE_CUSTOM => t('On custom page')
    ),
    '#default_value' => isset($items[$delta]['form']) ? $items[$delta]['form'] : REPLY_INHERIT
  );
  $element['subject'] = array(
    '#type' => 'radios',
    '#title' => t('Subject'),
    '#options' => array(
      REPLY_INHERIT => t('Inherit'),
      REPLY_SUBJECT_DISABLED => t('Hidden'),
      REPLY_SUBJECT_ALLOWED => t('Allowed'),
      REPLY_SUBJECT_REQUIRED => t('Required')
    ),
    '#default_value' => isset($items[$delta]['subject']) ? $items[$delta]['subject'] : REPLY_INHERIT
  );

  return $element;
}


/**
 * _field_widget_error
 */
function reply_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}


/**
 * _field_delete_instance
 */
function reply_field_delete_instance($instance) {
  // Zmazem vsetky reply, ktore patrili pod tuto instanciu.
  $ids = reply_get_instance($instance['id']);
  reply_delete_multiple($ids);
}